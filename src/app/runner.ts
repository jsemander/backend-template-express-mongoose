import * as moment from 'moment-timezone';
import { Models } from '../models';
import { Resolvers } from '../resolvers';
import { App, IApp, IAppConfig } from './';
import { Batch, IBatch } from './batch';
import { ITasks, Tasks } from './tasks';

/**
 * App Runner Configuration
 */
export const AppRunner = (dbUrl: string, configs: IAppConfig): Promise<IApp> => {
    return new Promise<IApp>((resolve, reject) => {
        /**
         * Connect to database and get data models
         */
        return Models(dbUrl).then((models) => {
            /**
             * Retrieve Configuration
             */
            return models.config.find({}).then((items) => {
                /**
                 * Pull Config from Database
                 */
                items.forEach((item) => configs[item.name] = item.value);
                
                /**
                 * Set default timezone for moment
                 */
                moment.tz.setDefault(configs.Timezone);
                
                /**
                 * Create resolver functions
                 */
                const resolvers = Resolvers(configs, models);
                
                /**
                 * Start Application as an API
                 */
                return resolve(App(configs, resolvers));
            });
        }).catch(reject);
    });
};

/**
 * Batch Runner Configuration
 */
export const BatchRunner = (dbUrl: string, configs: IAppConfig): Promise<IBatch> => {
    return new Promise<IBatch>((resolve, reject) => {
        /**
         * Connect to database and get data models
         */
        return Models(dbUrl).then((models) => {
            /**
             * Retrieve Configuration
             */
            return models.config.find({}).then((items) => {
                /**
                 * Pull Config from Database
                 */
                items.forEach((item) => configs[item.name] = item.value);
                
                /**
                 * Set default timezone for moment
                 */
                moment.tz.setDefault(configs.Timezone);
                
                /**
                 * Create resolver functions
                 */
                const resolvers = Resolvers(configs, models);
                
                /**
                 * Tasks for Scheduler
                 */
                const tasks: ITasks = Tasks(configs, resolvers);
                
                /**
                 * Start Application as an Batch worker
                 */
                return resolve(Batch(configs, resolvers, tasks));
            });
        }).catch(reject);
    });
};
