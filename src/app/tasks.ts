import * as moment from 'moment';
import { ITaskResponse } from '../lib/Types';
import { IResolvers } from '../resolvers';
import { IAppConfig } from './';

export interface ITasks {
    [key: string]: {
        [key: string]: (...args: any[]) => Promise<ITaskResponse>;
    };
    session: {
        clean: () => Promise<ITaskResponse>;
    };
}
export const Tasks = (configs: IAppConfig, resolvers: IResolvers): ITasks => {
    return {
        session: {
            clean: () => {
                return resolvers.session.remove({
                    expiration: {
                        $lte: moment().toISOString(),
                    },
                }).then(() => {
                    return {
                        success: true,
                    };
                });
            },
        },
    };
};
