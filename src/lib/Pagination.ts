import { Model } from 'mongoose';
import { IPagination } from './Types';

/**
 * Calculates the skip amount
 */
export const calculateSkip = (page: number, limit: number): number => (page - 1) * limit;

/**
 * Paginates the records
 */
export const paginateRecords = (
    model: Model<any>,
    params: any,
    sort: any,
    lookups: any[],
    unwinds: any[],
    page: number,
    limit: number,
): Promise<IPagination<any>> => {
    const skip = calculateSkip(page, limit);
    return model.countDocuments(params).then((total: number) => {
        const aggreagation = model.aggregate().match(model.where(params).cast(model));
        lookups.forEach((lookup) => aggreagation.lookup(lookup));
        unwinds.forEach((unwind) => aggreagation.unwind(unwind));
        aggreagation.sort(sort).skip(skip);
        if (limit > 0) {
            aggreagation.limit(limit);
        }
        return aggreagation.then((docs: any[]) => {
            return {
                data: docs,
                meta: {
                    data: {},
                    pagination: {
                        count: docs.length,
                        total,
                    },
                },
            };
        });
    });
};
