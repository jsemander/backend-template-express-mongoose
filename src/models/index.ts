import * as mongoose from 'mongoose';

import { ConfigSchema, IConfigModel } from './configs';
import { IJobModel, JobSchema } from './jobs';
import { ILogModel, LogSchema } from './logs';
import { IRoleModel, RoleSchema } from './roles';
import { ISessionModel, SessionSchema } from './sessions';
import { IUserModel, UserSchema } from './users';

export interface IModels {
    config: mongoose.Model<IConfigModel>;
    job: mongoose.Model<IJobModel>;
    log: mongoose.Model<ILogModel>;
    role: mongoose.Model<IRoleModel>;
    session: mongoose.Model<ISessionModel>;
    user: mongoose.Model<IUserModel>;
}
export const Models = (dbUrl: string): Promise<IModels> => {
    return new Promise<IModels>((resolve, reject) => {
        mongoose.connect(dbUrl, {
            useNewUrlParser: true,
        }, (err) => {
            if (err) {
                return reject(err);
            }
            return resolve({
                config: mongoose.model('Config', ConfigSchema),
                job: mongoose.model('Job', JobSchema),
                log: mongoose.model('Log', LogSchema),
                role: mongoose.model('Role', RoleSchema),
                session: mongoose.model('Session', SessionSchema),
                user: mongoose.model('User', UserSchema),
            });
        });
    });
};
