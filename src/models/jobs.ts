import { Document, Schema, Types } from 'mongoose';

export interface IJob {
    _id: Types.ObjectId;
    data: any;
    enabled: boolean;
    lastFinishedAt: Date;
    lastModifiedBy: string;
    lastRunAt: Date;
    name: string;
    nextRunAt: Date;
    repeatInterval: string;
    repeatTimezone: string;
    task: {
        model: string;
        action: string;
    };
}
export interface IJobModel extends Document {
    data: any;
    enabled: boolean;
    lastFinishedAt: Date;
    lastModifiedBy: string;
    lastRunAt: Date;
    name: string;
    nextRunAt: Date;
    repeatInterval: string;
    repeatTimezone: string;
    task: {
        model: string;
        action: string;
    };
}
export const JobSchema = new Schema({
    data: {
        default: {},
        type: Object,
    },
    enabled: {
        default: true,
        type: Boolean,
    },
    lastFinishedAt: {
        default: null,
        type: Date,
    },
    lastModifiedBy: {
        default: null,
        type: String,
    },
    lastRunAt: {
        default: null,
        type: Date,
    },
    name: {
        type: String,
    },
    nextRunAt: {
        default: null,
        type: Date,
    },
    repeatInterval: {
        default: null,
        type: String,
    },
    repeatTimezone: {
        default: 'America/New_York',
        type: String,
    },
    task: {
        action: {
            type: String,
        },
        model: {
            type: String,
        },
    },
}, {
    timestamps: true,
    versionKey: false,
});
