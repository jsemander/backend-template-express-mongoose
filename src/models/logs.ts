import { Document, Schema, Types } from 'mongoose';
import { IUser, IUserModel } from './users';

export interface ILog {
    _id: Types.ObjectId;
    action: string;
    data: any;
    entity: any;
    entity_name: string;
    error: any;
    level: string;
    type: string;
    updates: ILogUpdate[];
    user: IUser;
}
export interface ILogUpdate {
    field_name: string;
    new_value: any;
    old_value: any;
}
export interface ILogModel extends Document {
    [key: string]: any;
    action: string;
    data: any;
    entity?: any;
    entity_name: string;
    error: any;
    level: string;
    type: string;
    updates: ILogUpdate[];
    user: IUserModel | null;
}
export const LogUpdateSchema = new Schema({
    field_name: {
        type: String,
    },
    new_value: {
        type: Schema.Types.Mixed,
    },
    old_value: {
        type: Schema.Types.Mixed,
    },
}, {
    _id: false,
    versionKey: false,
});
export const LogSchema = new Schema({
    action: {
        required: true,
        type: String,
    },
    data: {
        default: {},
        required: true,
        type: Schema.Types.Mixed,
    },
    entity: {
        default: {},
        type: Schema.Types.Mixed,
    },
    entity_name: {
        required: true,
        type: String,
    },
    error: {
        default: {},
        type: Schema.Types.Mixed,
    },
    level: {
        default: 'data',
        required: true,
        type: String,
    },
    type: {
        default: 'info',
        required: true,
        type: String,
    },
    updates: [LogUpdateSchema],
    user: {
        default: null,
        ref: 'User',
        type: Schema.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
});
