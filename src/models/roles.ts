import { Document, Schema, Types } from 'mongoose';

export interface IRolePermission {
    access: boolean;
    create: boolean;
    level: string;
    entity: string;
    modify: boolean;
    name: string;
}
export interface IRole {
    _id: Types.ObjectId;
    createdAt: Date;
    enabled: boolean;
    name: string;
    permissions: IRolePermission[];
    updatedAt: Date;
}
export interface IRoleModel extends Document {
    createdAt: Date;
    enabled: boolean;
    name: string;
    permissions: IRolePermission[];
    updatedAt: Date;
}
export const RolePermissionSchema = new Schema({
    access: {
        default: false,
        type: Boolean,
    },
    create: {
        default: false,
        type: Boolean,
    },
    entity: {
        required: true,
        type: String,
    },
    level: {
        required: true,
        type: String,
    },
    modify: {
        default: false,
        type: Boolean,
    },
    name: {
        required: true,
        type: String,
    },
}, {
    _id: false,
    versionKey: false,
});
export const RoleSchema = new Schema({
    enabled: {
        default: true,
        type: Boolean,
    },
    name: {
        required: true,
        type: String,
    },
    permissions: [RolePermissionSchema],
}, {
    timestamps: true,
    versionKey: false,
});
