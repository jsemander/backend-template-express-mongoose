import { Document, Schema, Types } from 'mongoose';
import { IRole, IRoleModel } from './roles';

export interface IUser {
    _id: Types.ObjectId;
    createdAt: Date;
    email: string;
    enabled: boolean;
    first_name: string;
    last_name: string;
    middle_name?: string;
    name: string;
    role: IRole | null;
    updatedAt: Date;
    username: string;
}
export interface IUserModel extends Document {
    createdAt: Date;
    email: string;
    enabled: boolean;
    first_name: string;
    last_name: string;
    middle_name?: string;
    name: string;
    role: IRoleModel | null;
    updatedAt: Date;
    username: string;
}
export const UserSchema = new Schema({
    email: {
        required: true,
        type: String,
    },
    enabled: {
        default: true,
        type: Boolean,
    },
    first_name: {
        required: true,
        type: String,
    },
    last_name: {
        required: true,
        type: String,
    },
    middle_name: {
        default: '',
        type: String,
    },
    role: {
        default: null,
        ref: 'Role',
        type: Schema.Types.ObjectId,
    },
    username: {
        required: true,
        type: String,
    },
    verified: {
        default: false,
        type: Boolean,
    },
}, {
    timestamps: true,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
    versionKey: false,
});
UserSchema.virtual('name').get(function(this: IUser) {
    return this.first_name + ' ' + (this.middle_name ? this.middle_name + ' ' : '') + this.last_name;
});
