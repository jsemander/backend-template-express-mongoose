import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { IUserModel } from '../models/users';

export interface IUserCreateParams {
    email: string;
    first_name: string;
    last_name: string;
    middle_name?: string;
    role: string | null;
    username: string;
    verified: boolean;
}
export interface IUserListParams {
    $or?: Array<{
        email?: string | RegExp;
        first_name?: string | RegExp;
        last_name?: string | RegExp;
        username?: string | RegExp;
    }>;
    enabled?: boolean;
    role?: {
        $eq?: string | null;
        $in?: string | null[];
    };
    verified?: boolean;
}
export interface IUserUpdateParams {
    email?: string;
    enabled?: boolean;
    first_name?: string;
    last_name?: string;
    middle_name?: string;
    role?: string | null;
    username?: string;
    verified?: boolean;
}
export class User extends Resolver<IModels> {
    /**
     * Create user
     */
    public create(data: IUserCreateParams): Promise<IUserModel> {
        return this.getByUsername(data.username).then((result) => {
            if (result) {
                return Promise.reject<any>({
                    message: 'A user already exists by the specified username',
                    statusCode: 409,
                });
            }
            const user = new this.models.user(data);
            return user.save().then((record) => this.get(record._id));
        });
    }
    
    /**
     * Get user by id
     */
    public get(id: string): Promise<IUserModel> {
        return this.models.user.findById(id).populate({
            path: 'role',
        }).then((user) => {
            if (!user) {
                return Promise.reject<IUserModel>({
                    message: 'No user found',
                    statusCode: 404,
                });
            }
            return user;
        });
    }
    
    /**
     * Get user by username
     */
    public getByUsername(username: string): Promise<IUserModel | null> {
        return this.models.user.findOne({
            username: new RegExp(`^${username}$`, 'i'),
        }).then((user) => user);
    }
    
    /**
     * Get user by id and validates that they are authorized
     */
    public isAuthorized(id: string): Promise<IUserModel> {
        return this.get(id).then((user) => {
            if (!user || !user.enabled || !user.role) {
                return Promise.reject<any>({
                    message: 'Your username/password combination is incorrect',
                    statusCode: 403,
                });
            }
            return user;
        });
    }
    
    /**
     * Retrieves a list of users by params
     */
    public list(params: IUserListParams, options: IPaginationOptions): Promise<IPagination<IUserModel>> {
        return paginateRecords(this.models.user, params, options.sort, [{
            as: 'role',
            foreignField: '_id',
            from: 'roles',
            localField: 'role',
        }], [{
            path: '$role',
            preserveNullAndEmptyArrays: true,
        }], options.page, options.limit);
    }
    
    /**
     * Update user
     */
    public update(id: string, data: IUserUpdateParams): Promise<IUserModel> {
        return this.models.user.findById(id).then((record) => {
            // Check if the record was found
            if (!record) {
                return Promise.reject<any>({
                    message: 'No user found',
                    statusCode: 404,
                });
            }
            
            // Set the user
            record.set(data);
                    
            // Return the user after saving
            return record.save().then(() => this.get(id));
        });
    }
}
