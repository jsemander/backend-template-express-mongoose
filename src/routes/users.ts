import { NextFunction, Request, Response, Router } from 'express';
import { compact, isEqual, map } from 'lodash';
import { IPaginationOptions } from '../lib/Types';
import { IResolvers } from '../resolvers';
import { IUserListParams } from '../resolvers/user';

export default (resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /users List Paginated Users
     * @apiGroup User
     * @apiName List Paginated Users
     * @apiSuccess {Object[]} users Array of users
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: IUserListParams = {};
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: {
                name: 1,
            },
        };
        
        if (req.query.page) {
            options.page = parseInt(req.query.page,  10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit,  10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.roles) {
            if (typeof req.query.roles === 'string') {
                params.role = {
                    $in: [req.query.roles],
                };
            } else {
                params.role = {
                    $in: req.query.roles,
                };
            }
        } else if (req.query.role === 'No Access') {
            params.role = {
                $eq: null,
            };
        } else if (req.query.role) {
            params.role = {
                $eq: req.query.role,
            };
        }
        return resolvers.user.list(params, options).then((users) => {
            return res.json(users);
        }).catch(next);
    });
    
    /**
     * @api {get} /users/:id Get user by id
     * @apiGroup User
     * @apiName Get user by id
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.get('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.user.get(req.params.id).then((users) => {
            return res.json(users);
        }).catch(next);
    });
    
    /**
     * @api {post} /users Create user
     * @apiGroup User
     * @apiName Create user
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.post('/', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.user.create(req.body).then((user) => {
            return resolvers.log.create({
                action: 'create',
                data: {},
                entity: user._id,
                entity_name: 'user',
                type: 'info',
                updates: map(user.toObject(), (newValue, fieldName) => {
                    switch (fieldName) {
                        default:
                            return {
                                field_name: fieldName,
                                new_value: newValue,
                                old_value: null,
                            };
                    }
                }),
                user: req.user._id,
            }).then(() => res.json(user));
        }).catch(next);
    });
    
    /**
     * @api {put} /users/:id Update user by Id
     * @apiGroup User
     * @apiName Update user by Id
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    router.put('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.user.get(req.params.id).then((record) => {
            const original = record.toObject();
            return resolvers.user.update(req.params.id, req.body).then((updatedRecord) => {
                const user = updatedRecord.toObject();
                return resolvers.log.create({
                    action: 'update',
                    data: {},
                    entity: user._id,
                    entity_name: 'user',
                    type: 'info',
                    updates: compact(map(original, (oldValue, fieldName) => {
                        if (!isEqual(oldValue, user[fieldName])) {
                            return {
                                field_name: fieldName,
                                new_value: user[fieldName],
                                old_value: oldValue,
                            };
                        }
                    })),
                    user: req.user._id,
                }).then(() => res.json(user));
            });
        }).catch(next);
    });
    
    return router;
};
