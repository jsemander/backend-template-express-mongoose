import { assert, expect } from 'chai';
import 'mocha';
import * as mongoose from 'mongoose';
import { Models } from '../../src/models';
import TestDbHelper from '../helpers/db';

const db = new TestDbHelper();

describe('Models:index', () => {
    before(() => db.start());
    after(() => db.stop());
    it('should connect to server', () => {
        return db.getConnectionString().then((uri) => {
            return Models(uri).then(() => {
                return mongoose.disconnect();
            });
        });
    });
    it('should throw an error', () => {
        return Models('mongodb://foo:27017/bar').then(() => {
            assert.fail('Mongoose should not connect to mongodb');
        }).catch((err) => {
            expect(err.message).to.match(/^failed to connect to server/);
        });
    });
});
