import { assert, expect } from 'chai';
import 'mocha';
import * as moment from 'moment-timezone';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:Log', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => models.log.deleteMany({}));
    describe('#get()', () => {
        it('should find a record', () => {
            return resolvers.log.create({
                action: 'shutdown',
                data: {},
                entity: null,
                entity_name: 'users',
                error: {},
                type: 'error',
                updates: [],
            }).then((l) => {
                return resolvers.log.get(l._id).then((l2) => {
                    expect(l2).to.have.property('entity_name', 'users');
                    expect(l2).to.have.property('level', 'data');
                });
            });
        });
        it('should not find any record', () => {
            return resolvers.log.get('5a3c861b6d4ef006979bf674').then(() => {
                assert.fail('Log should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No log found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should find one of two records', () => {
            return Promise.all([
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'system',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'email',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return resolvers.log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 1,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
        it('should find all records up to 25', () => {
            return Promise.all([
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'system',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'email',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return resolvers.log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 25,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
        it('should find all records', () => {
            return Promise.all([
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'api',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
                resolvers.log.create({
                    action: 'shutdown',
                    data: {},
                    entity: null,
                    entity_name: 'cron',
                    error: {},
                    type: 'error',
                    updates: [],
                }),
            ]).then(() => {
                return resolvers.log.list({
                    createdAt: {
                        $gte: Date.parse(moment().startOf('day').toISOString()),
                        $lte: Date.parse(moment().endOf('day').toISOString()),
                    },
                }, {
                    limit: 0,
                    page: 1,
                    sort: {
                        createdAt: -1,
                    },
                }).then((records) => {
                    expect(records.data.length).to.equal(records.meta.pagination.count);
                });
            });
        });
    });
});
