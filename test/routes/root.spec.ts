import * as chai from 'chai';
import chaiHttp = require('chai-http');
import * as faker from 'faker';
import 'mocha';
import * as mongoose from 'mongoose';
import { App, IApp, IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

chai.use(chaiHttp);

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let app: IApp;
let models: IModels;
let resolvers: IResolvers;

describe('Routes:Root', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    // Get models
                    models = m;
                    // Create resolvers
                    resolvers = Resolvers(configs, models);
                    // Start up application
                    app = App(configs, resolvers);
                });
            });
        });
    });
    after(() => {
        return new Promise((resolve) => {
            app.server.close(() => {
                return mongoose.disconnect().then(() => db.stop().then(() => {
                    return resolve();
                }));
            });
        });
    });
    afterEach(() => Promise.all([
        models.log.deleteMany({}),
        models.role.deleteMany({}),
        models.session.deleteMany({}),
        models.user.deleteMany({}),
    ]));
    it('should get the application information from GET / route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD,
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 200);
                        chai.expect(res.body).to.have.property('token');
                        return chai.request(app.server).get('/').set('Authorization', `Bearer ${res.body.token}`).send().then((res2) => {
                            chai.expect(res2).to.have.property('status', 200);
                            chai.expect(res2).to.have.property('type', 'text/plain');
                            chai.expect(res2.text).to.be.equal(`${configs.Name} v${configs.Version}`);
                            return resolve();
                        });
                    });
                });
            });
        });
    });
    it('should get the robots.txt from GET /robots.txt route', () => {
        return new Promise((resolve) => {
            // Run Request
            return chai.request(app.server).get('/robots.txt').send().then((res) => {
                chai.expect(res).to.have.property('status', 200);
                chai.expect(res).to.have.property('type', 'text/plain');
                chai.expect(res.text).to.be.equal('UserAgent: *\nDisallow: /');
                return resolve();
            });
        });
    });
    it('should not get the current user information from GET /me route', () => {
        return new Promise((resolve) => {
            // Run Request
            return chai.request(app.server).get('/me').send().then((res) => {
                chai.expect(res).to.have.property('status', 401);
                chai.expect(res.body).to.have.property('message', 'Please make sure your request has an Authorization header');
                return resolve();
            });
        });
    });
    it('should get the current user information from GET /me route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD,
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 200);
                        chai.expect(res.body).to.have.property('token');
                        return chai.request(app.server).get('/me').set('Authorization', `Bearer ${res.body.token}`).send().then((res2) => {
                            chai.expect(res2).to.have.property('status', 200);
                            chai.expect(res2.body).to.have.property('name', user.toObject().name);
                            return resolve();
                        });
                    });
                });
            });
        });
    });
    it('should get the application information from POST / route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD,
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 200);
                        chai.expect(res.body).to.have.property('token');
                        return chai.request(app.server).post('/').set('Authorization', `Bearer ${res.body.token}`).send().then((res2) => {
                            chai.expect(res2).to.have.property('status', 200);
                            chai.expect(res2).to.have.property('type', 'text/plain');
                            chai.expect(res2.text).to.be.equal(`${configs.Name} v${configs.Version}`);
                            return resolve();
                        });
                    });
                });
            });
        });
    });
    it('should not let the user login from POST /login route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD + '!',
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 401);
                        chai.expect(res.body).to.have.property('message', 'Your username/password combination is incorrect');
                        return resolve();
                    });
                });
            });
        });
    });
    it('should let the user login from POST /login route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD,
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 200);
                        chai.expect(res.body).to.have.property('token');
                        return resolve();
                    });
                });
            });
        });
    });
    it('should get the robots.txt from POST /robots.txt route', () => {
        return new Promise((resolve) => {
            // Run Request
            return chai.request(app.server).post('/robots.txt').send().then((res) => {
                chai.expect(res).to.have.property('status', 200);
                chai.expect(res).to.have.property('type', 'text/plain');
                chai.expect(res.text).to.be.equal('UserAgent: *\nDisallow: /');
                return resolve();
            });
        });
    });
    it('should get the application information from PUT / route', () => {
        return resolvers.role.create({
            name: 'Admin',
            permissions: [],
        }).then((role) => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: role._id,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return new Promise((resolve) => {
                    // Run Request
                    return chai.request(app.server).post('/login').send({
                        password: process.env.TEST_PASSWORD,
                        username: user.username,
                    }).then((res) => {
                        chai.expect(res).to.have.property('status', 200);
                        chai.expect(res.body).to.have.property('token');
                        return chai.request(app.server).put('/').set('Authorization', `Bearer ${res.body.token}`).send().then((res2) => {
                            chai.expect(res2).to.have.property('status', 200);
                            chai.expect(res2).to.have.property('type', 'text/plain');
                            chai.expect(res2.text).to.be.equal(`${configs.Name} v${configs.Version}`);
                            return resolve();
                        });
                    });
                });
            });
        });
    });
    it('should get the robots.txt from PUT /robots.txt route', () => {
        return new Promise((resolve) => {
            // Run Request
            return chai.request(app.server).put('/robots.txt').send().then((res) => {
                chai.expect(res).to.have.property('status', 200);
                chai.expect(res).to.have.property('type', 'text/plain');
                chai.expect(res.text).to.be.equal('UserAgent: *\nDisallow: /');
                return resolve();
            });
        });
    });
});
